/**        
 * START HERE
 * ==========
 *
 * - Use vanilla Javascript
 * - The code below is not perfect, we expect you to improve some of it.
 * - Show in the code that you have some knowledge of ES6
 *
 * ASSIGNMENT - NETFLIX LIKE CATALOGUE
 * ===================================
 * 1. Create 3 rows. One row for every genre. ['Action', 'Crime', 'Drama'] DONE
 * 2. Create a movie poster selection indicator. (This can be a line, a border, a colored rectangle, etc. Use your imagination.) DONE
 * 3. Add up/down keys for selection between the different rows. DONE
 * 4. Animate the rows when the user presses up/down DONE
 *
 * Good luck!
 */

class App {
	static async _fetchMovies(datasetLocation) {
		let response = await fetch(datasetLocation)
		return await response.json();
	}

	static createAssetsProxy(movies) {
		if (movies === undefined) throw new Error('movies is undefined');
		
		return new Proxy(movies, {
			get: (target, key) => {
				return target.data.reduce((accumulator, item) => {
					accumulator.push(...item.assets);
					return accumulator;
				}, []).filter(asset => asset.genre.toLowerCase() === key.toLowerCase());
			}
		})
	}

	static selectCarousel(carouselId) {	
		this.carousels.forEach((carousel) => {
			carousel.deselectAll();
			carousel.setAsSelected(false);
		});
		
		this.selectedCarousel = carouselId;
		this.carousels[carouselId].reselectItem();
		this.carousels[carouselId].setAsSelected(true);
	}

	static getSelectedCarousel() {
		return this.carousels[this.selectedCarousel];
	}

	static _initCarouselSelectorHandlers() {
		document.querySelector("body")
			.addEventListener('keydown', evt => {
				evt = evt || window.event;
				switch (evt.keyCode) {
					case 37:
						this.getSelectedCarousel().next();
						break;

					case 39:
						this.getSelectedCarousel().previous();
						break;

					case 38:
						this.selectedCarousel > 0 ? this.selectedCarousel-- : this.selectedCarousel = 0;
						this.selectCarousel(this.selectedCarousel);
						break;

					case 40:
						this.selectedCarousel < this.totalCarousels - 1 ? this.selectedCarousel++ : this.selectedCarousel = this.totalCarousels - 1;
						this.selectCarousel(this.selectedCarousel);
						break;
				}
			})
	}

	// function name instead of self executing method for future unit testing purposes
	// so function can be called manually
	static init(movieStyles, datasetLocation) {
		if (movieStyles === undefined) throw new Error("Passing an array of movieStyles is required");
		if (datasetLocation === undefined) throw new Error("Passing a location for the movies dataset is required");

		this.selectedCarousel = 0;
		this.totalCarousels = movieStyles.length;
		this.movieStyles = movieStyles;
		this.carousels = []

		this._fetchMovies(datasetLocation)
			.then(movies => {
				const assets = this.createAssetsProxy(movies);

				this.movieStyles.forEach((movieContainer, index) => {
					this.carousels[index] = new Carousel(this, document.querySelector(`.${movieContainer}`), assets[movieContainer], movieContainer, index)
				})

				this.selectCarousel(this.selectedCarousel)
				this._initCarouselSelectorHandlers();
			})
			.catch(reason => console.log(reason.message));
	}
}