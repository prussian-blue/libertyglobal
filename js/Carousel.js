class Carousel {
	constructor(appReference, element, data, movieContainer, index) {
		this.element = element;
		this.data = data;
		this.selectedElement;

		this.element.appendChild(document.createElement('h2')).innerHTML = movieContainer;
		this.element.appendChild(document.createElement('div')).className = 'background';

		this.data.forEach((asset) => {
			let el = this.element.appendChild(document.createElement('asset'));
			el.appendChild(document.createElement('div')).className = 'overlay'

			el.addEventListener('click', (evt) => {
				this.selectedElement = evt.target.getAttribute('data-title');
				this.resetSelectedElements();
				appReference.selectCarousel(index);
			})
		});

		this.alignDataset();
	}

	deselectAll() {
		this.element.querySelectorAll('asset').forEach((child) => {
			child.className = '';
		})
	}

	reselectItem () {
		let done = false;

		//if one element is selected already, then select it
		this.element.querySelectorAll('asset').forEach((child) => {
			if (child.getAttribute('data-title') === this.selectedElement) {
				child.className = 'selected';
				done = true;
				return
			}
		})

		if (done)	return;

		//otherwise, select the first item. this is needed for keyboard carousel navigation
		this.selectedElement = this.element.querySelector('asset').getAttribute('data-title')
		this.element.querySelector('asset').className = 'selected';
	}

	setAsSelected(isSelected) {
		if (isSelected) {
			this.element.className += ' selected';
		}	else {
			this.element.className = this.element.className.replace(/selected/gi, '');
		}
	}

	resetSelectedElements() {
		this.deselectAll();
		this.reselectItem();
	}

	next() {
		this.currentIndex++;
		this.alignDataset();
	}

	previous() {
		this.currentIndex--;
		this.alignDataset();
	}

	get currentIndex() {
		return parseInt(this.element.dataset.currentindex) || 0;
	}

	set currentIndex(val) {
		if (val < 0) val = this.data.length - 1;
		else if (val >= this.data.length) val = 0;
		this.element.dataset.currentindex = val;
	}

	alignDataset() {
		[...this.element.children].forEach((assetElement, i) => {
			if (assetElement.nodeName !== 'ASSET') return;

			const index = (i + this.currentIndex) % this.data.length;
			assetElement.dataset.title = this.data[index].title;
			assetElement.style.backgroundImage = `url(${this.data[index].img})`;
		});

		this.resetSelectedElements();
	}
}